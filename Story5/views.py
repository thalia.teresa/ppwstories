from django.shortcuts import render, redirect
from .models import bookschedule
from .forms import FormJadwal


# Create your views here.
def jadwal(request):
    page_jadwal = bookschedule.objects.all()
    form = FormJadwal()
    if request.method == 'POST':
        form = FormJadwal(request.POST)
        if form.is_valid():
            InputTitle = request.POST['InputTitle']
            category = request.POST['category']
            date_start = request.POST['date_start']
            date_end = request.POST['date_end']
            InputLocation = request.POST['location']
            InputDescription = request.POST['description']
            addSchedule = bookschedule(InputTitle=InputTitle, category=category, date_start=date_start, date_end=date_end, InputLocation=InputLocation, InputDescription=InputDescription,   )
            addSchedule.save()
            return redirect('Story5:jadwal')

    return render(request, 'index.html', {
        'page_jadwal': page_jadwal, 'form': form
    })

def tambahJadwal(request):
    if request.method == 'POST':
        form = FormJadwal(request.POST)
        print(request.POST['InputDescription'])
        if form.is_valid():
            InputTitle = request.POST['InputTitle']
            category = request.POST['category']
            date_start = request.POST['date_start']
            date_end = request.POST['date_end']
            InputLocation = request.POST['InputLocation']
            InputDescription = request.POST['InputDescription']
            addSchedule = bookschedule(InputTitle=InputTitle, category=category, date_start=date_start, date_end=date_end, InputLocation=InputLocation, InputDescription=InputDescription,   )
            addSchedule.save()
    return redirect('Story5:jadwal')

def hapusJadwal(request,id):
    if request.method == 'POST':

        # hapus = bookschedule.objects.get(id=request.POST['id'])
        # hapus.delete()
        bookschedule.objects.get(id=id).delete()
    return redirect('Story5:jadwal')
