from django import forms
from .models import bookschedule

class FormJadwal(forms.Form):
    InputTitle = forms.CharField(max_length=50, required=True)
    category = forms.CharField(max_length=30)
    date_start = forms.DateTimeField(
        required=True, input_formats=['%Y-%m-%dT%H:%M'])
    date_end = forms.DateTimeField(
        required=True, input_formats=['%Y-%m-%dT%H:%M'])
    InputLocation = forms.CharField(max_length=50)
    InputDescription = forms.CharField(max_length=80, required=False)
    """ class Meta:
        model = models.bookschedule
        fields = [
            'InputTitle',
            'category',
            'date_start',
            'date_end',
            'InputLocation',
            'InputDescription',
        ] """


""" from django import forms
from . import models
from django.utils import timezone
from datetime import datetime

class TimeInput(forms.TimeInput):
    input_type = "time"

class DateInput(forms.DateInput):
    input_type = "date"
    input_formats = ['%d %b %Y']


class FormJadwal(forms.ModelForm):
    class Meta:
        model = models.bookschedule
        fields = ['InputTitle', 'category', 'date_start', 'date_end', 'InputLocation', 'InputDescription']

         widgets = {
            "time": TimeInput,
            "date": DateInput
        }
  """