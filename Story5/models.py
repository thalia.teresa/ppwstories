from django.db import models

# Create your models here.
class bookschedule(models.Model):
    InputTitle = models.CharField(max_length=50)
    category = models.CharField(max_length=30, default="Task")
    date_start = models.DateTimeField(blank=True, null=True)
    date_end = models.DateTimeField(blank=True, null=True)
    InputLocation = models.CharField(max_length=50)
    InputDescription = models.CharField(max_length=80, blank=True)
    idNumber = models.CharField(max_length=100)